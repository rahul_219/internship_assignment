from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from testapp.views import Studentdata, InsertData, DeleteData, UpdateData



urlpatterns = [
	#url(r'^sum/$', csrf_exempt(testapp.as_view()),name="test-sum"),
	url(r'^get_req/', Studentdata.as_view(),name='s'),
    url(r'^post_req/', InsertData.as_view(),name='v'),
	url(r'^delete_req/(?P<id>\d+)$', DeleteData.as_view(),name='l'),
	url(r'^put_req/',UpdateData.as_view(),name='u'),
]
