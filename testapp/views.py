from django.shortcuts import render
from django.http import HttpResponse, QueryDict
from django.views.generic import View
from testapp.models import student
from django.core.exceptions import ObjectDoesNotExist

'''
class maths(View):
    def get(request):
    #html = "<html> <body> 2 + 2 = 4 </body> </html>"
    return HttpResponse('2 + 2 =4', status=200)
'''''
#for getting data
class Studentdata(View):
    def get(self, request):
        answer = []
        student_val = student.objects.all()
        for val in student_val:
            ans = {}
            ans['first_name'] = val.first_name
            ans['last_name'] = val.last_name
            ans['roll_num'] = val.roll_num
            ans['address'] = val.address
            answer.append(ans)
        return HttpResponse(answer, status=200)

#for inserting new data into database
class InsertData(View):
    def post(self, request):
        first_name = request.POST["first_name"]
        last_name = request.POST["last_name"]
        roll_num = request.POST["roll_num"]
        address = request.POST["address"]
        student_info = student(first_name=first_name, last_name=last_name, roll_num=roll_num, address=address)
        student_info.save()
        return HttpResponse("data_inserted")

#for deleting an id from database
class DeleteData(View):
    def delete(self,request,id):
        #data = QueryDict(request.body)
        try:
            student.objects.get(id=id).delete()
            #value.delete()
            return HttpResponse("deleted successfully")
        except ObjectDoesNotExist:
            #return HttpResponse("college_id " + data.get('roll_num') + " does not exist ")
            return HttpResponse("college_id does not exist in DataBase")

#for the updation of our database
class UpdateData(View):
    def put(self,request):
        data = QueryDict(request.body)
        try:
            update_info = student.objects.get(roll_num = data.get('roll_num'))
            if data.get('first_name'):
               update_info.first_name = data.get('first_name')
            if data.get('last_name'):
               update_info.last_name = data.get('last_name')
            if data.get('roll_num'):
               update_info.roll_num = data.get('roll_num')
            if data.get('address'):
               update_info.address = data.get('address')
            update_info.save()
            return HttpResponse("updated successfully")
        except ObjectDoesNotExist:
            return HttpResponse("college_id " + data.get('roll_num') + " does not exist ")
