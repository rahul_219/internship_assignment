
                                             Running of Code

      To run the code there are some  prerequisite
            1-Python 2.7
            2-Django 1.8
            3-postman
            4- Sqllite

                   Create records, delete records and update records will be done using postman.

        1. For running the project just go to the directory where your project.

         2. Create virtual environment

         3. Run your virtual environment by command source virtual_env name/bin/activate

         4. And at last run your server by command python manage.py runserver

          5. Now in my project there are several methods named get, post, delete, put.

        6. For fetching information : - http://127.0.0.01:8081/yoyo/get_req

        7. For insertion or creation of new entries :- http://127.0.0.01:8081/yoyo/post_req

        8:- for deletion of entries :- http://127.0.0.01:8081/yoyo/delete_req

        9:- for updation of data : - http://127.0.0.01:8081/yoyo/put_req
